package mx.itesm.fj2;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

// the helper is the class in charge of dealing with the db
public class DBHelper extends SQLiteOpenHelper {

    private static final String DB_FILE = "StudentDatabase.db";
    private static final String TABLE = "students";
    private static final String FIELD_ID = "id";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_STUDENT_ID = "student_id";

    public DBHelper(Context context){

        super(context, DB_FILE, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // let's create the table in the DB
        String query = "CREATE TABLE " + TABLE + "(" +
                    FIELD_ID + " INTEGER PRIMARY KEY, " +
                    FIELD_NAME + " TEXT, " +
                    FIELD_STUDENT_ID + " TEXT)";

        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // do whatever logic you deem necessary when changing your versions
        // very simple choice - recreate db

        // drop the DB!
        // prepared statements - a way to parametrize a query
        String query = "DROP TABLE IF EXISTS ?";
        String[] args = {TABLE};

        db.execSQL(query, args);

        onCreate(db);

    }

    public void save(String name, String studentId){

        // irk

        // getting a reference to the db we're working with
        SQLiteDatabase db = getWritableDatabase();

        // ContentValues - a container of values
        // that works in a similar fashion to a dictionary
        // (pairs - key, value)
        ContentValues values = new ContentValues();

        values.put(FIELD_NAME, name);
        values.put(FIELD_STUDENT_ID, studentId);

        db.insert(TABLE, null, values);
    }

    public int delete(String name){

        SQLiteDatabase db = getWritableDatabase();
        String clause = FIELD_NAME + " = ?";
        String[] args = {name};

        return db.delete(TABLE, clause, args);

    }

    public String search(String studentId){

        SQLiteDatabase db = getReadableDatabase();
        String clause = FIELD_STUDENT_ID + " = ?";
        String[] args = {studentId};


        // returns a cursor
        Cursor c = db.query(TABLE, null, clause, args, null, null, null);
        String result = null;

        // move to first moves the cursor to first record
        // if the result is an empty set then it returns false
        if(c.moveToFirst()){

            // how to get a particular value in the current row
            int id = c.getInt(0);
            result = c.getString(1);
            String sid = c.getString(2);

            // traverse the whole result structure
            do {

                // do your logic here!
            } while(c.moveToNext());
        }


        return result;

    }
}
