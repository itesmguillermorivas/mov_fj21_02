package mx.itesm.fj2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SharedPrefsActivity extends AppCompatActivity {

    // local XML file that contains a set of values
    // saved in key-value form

    private static final String PREFS_FILE = "myPrefs";
    private static final String KEY_NAME = "name";
    private static final String KEY_LAST_NAME = "last_name";

    private SharedPreferences prefs;
    private EditText name, lastName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shared_prefs);

        name = findViewById(R.id.shared_name);
        lastName = findViewById(R.id.shared_last_name);

        // load file to object
        prefs = getSharedPreferences(PREFS_FILE, MODE_PRIVATE);
    }

    public void save(View v){

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(KEY_NAME, name.getText().toString());
        editor.putString(KEY_LAST_NAME, lastName.getText().toString());
        editor.commit();

        Toast.makeText(this, "SAVING VALUES TO OBJECT", Toast.LENGTH_SHORT).show();
    }

    public void showValues(View v){

        Toast.makeText(
                this,
                "VALUES - NAME: " +
                        prefs.getString(KEY_NAME, "NO NAME") +
                        " LAST NAME: " +
                        prefs.getString(KEY_LAST_NAME, "NO LAST NAME"),
                Toast.LENGTH_LONG
        ).show();
    }

    public void deleteName(View v){

        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(KEY_NAME);
        editor.commit();

        Toast.makeText(this, "NAME HAS BEEN DELETED", Toast.LENGTH_SHORT).show();
    }

    public void deleteEverything(View v){

        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();

        Toast.makeText(this, "SHAREDPREFS EMPTY", Toast.LENGTH_SHORT).show();
    }
}