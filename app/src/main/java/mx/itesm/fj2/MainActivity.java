package mx.itesm.fj2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText name, studentId;
    private DBHelper db;

    // today we will work with local storage
    // - local db with sqlite
    // - sharedprefs

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = findViewById(R.id.name);
        studentId = findViewById(R.id.student_id);
        db = new DBHelper(this);
    }

    public void add(View v){

        db.save(name.getText().toString(), studentId.getText().toString());
        Toast.makeText(this, "SAVING...", Toast.LENGTH_SHORT).show();
    }

    public void delete(View v){

        int result = db.delete(name.getText().toString());
        Toast.makeText(this, "ROWS AFFECTED: " + result, Toast.LENGTH_SHORT).show();
    }

    public void search(View v){

        String n = db.search(studentId.getText().toString());
        name.setText(n);
        Toast.makeText(this, "SEARCHING...", Toast.LENGTH_SHORT).show();
    }

    public void prefs(View v){

        Intent i = new Intent(this, SharedPrefsActivity.class);
        startActivity(i);
    }
}